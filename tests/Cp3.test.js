const chapter3 = require("../pages/Chapter3.page");

describe("Suite of Chapter 3", function(){
beforeEach( ()=>{
    browser.url("/");
    browser.maximizeWindow();        
});

    it("How to over", () =>{
        chapter3.clickOnLink(25);
        chapter3.hoverImage(3);
        expect(chapter3.imageDetails(3).getText()).to.equal("name: user1");
        chapter3.hoverImage(4);
        expect(chapter3.imageDetails(4).getText()).to.equal("name: user2");
        chapter3.hoverImage(5);
        expect(chapter3.imageDetails(5).getText()).to.equal("name: user3");
    });
    
    it("Sending keys to the browser", () =>{
        chapter3.clickOnLink(31);
        chapter3.sendKeys("Tab");        
        expect(chapter3.resultKeys.getText()).to.equal("You entered: TAB");
    });
    
    it("Scroll to element using MoveTo", () =>{
        chapter3.scrollUsingMove();
    }); 

    it("Scroll to element using Scroll to element", () =>{
        chapter3.scrollUsingScroll();
    });
    
    it("Switch Window", () =>{
        chapter3.clickOnLink(33);
        chapter3.switchWindow();
    });
    
    it("Switch to iFrame", () =>{
        chapter3.clickOnLink(22);
        chapter3.switchToiFrame("Insert text in the iFrame");
    });
    //This test fail on the training site.
    it.skip("Drang And Drop", () =>{
        chapter3.clickOnLink(10);
        chapter3.dragDrop(2);
    });
    
    it("Drang And Drop 2", () =>{
        browser.url("https://crossbrowsertesting.github.io/drag-and-drop.html");
        chapter3.dragDrop2();
    });   
});