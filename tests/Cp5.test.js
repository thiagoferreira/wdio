const chapter3 = require("../pages/Chapter3.page");
const chapter5 = require("../pages/Chapter5.page");

describe("Suite of Chapter 5", function(){
beforeEach( ()=>{
    browser.url("/");
    browser.maximizeWindow();        
});

    it("Check if teh field is enabled", () =>{
        chapter3.clickOnLink(13);
        chapter5.checkEnabledField();
    });

    it("Wait Until Example", () =>{
        chapter3.clickOnLink(13);
        chapter5.waitUntilExample();
    });
});