const chapter3 = require("../pages/Chapter3.page");
const chapter4 = require("../pages/Chapter4.page");

describe("Suite of Chapter 4", function(){
beforeEach( ()=>{
    browser.url("/");
    browser.maximizeWindow();        
});

    it("Select Dropdown", () =>{
        chapter3.clickOnLink(11);
        chapter4.dropDown(2);
    });

    it("JS Alert", () =>{
        chapter3.clickOnLink(29);
        chapter4.jsAlerts();
    });

    it("JS Alert Confirm", () =>{
        chapter3.clickOnLink(29);
        chapter4.jsAlertConfirm();
    });

    it("JS Alert Dismiss", () =>{
        chapter3.clickOnLink(29);
        chapter4.jsAlertDismiss();
    });

    it("JS Alert Dialog", () =>{
        chapter3.clickOnLink(29);
        chapter4.jsAlertDialog("teste");
        browser.pause(1000);
    });
});