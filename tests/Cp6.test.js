const chapter6 = require("../pages/Chapter6.page");

describe("Suite of Chapter 6", function(){
beforeEach( ()=>{
    browser.url("/");
    browser.maximizeWindow();        
});

    it("Login Using Data", () =>{
        browser.url("/login");
        chapter6.loginWithData();
    });
});