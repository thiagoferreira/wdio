class Chapter5{

    get enableButton () {return $("form#input-example button")};
    get enableInput () {return $("form#input-example input")};
    get removeAddBtn () {return $("form#checkbox-example button")};

    checkEnabledField(){
        this.enableButton.waitForDisplayed();
        this.enableInput.waitForEnabled(1000,true);
        this.enableButton.click();
        this.enableInput.waitForEnabled(4000);        
    }

    waitUntilExample(){
        this.removeAddBtn.waitForDisplayed();
        this.removeAddBtn.click();
        browser.waitUntil(() => {
            return this.removeAddBtn.getText() === 'Add'
        }, 6000 , 'Expect button text to change');
    }
}

module.exports = new Chapter5();