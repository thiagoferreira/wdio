class Chapter2 {

    get pageFooter() {return $('#page-footer')};
    get parent () {return $('ul')};
    get childElements() {return this.parent.$$('li')};
    specificChildElement(index) {return this.parent.$(`li:nth-child(${index})`)};
    
    getLiText(){
        this.childElements.filter((element) =>{
            console.log(element.getText());
        });   
    }

    getspecificChildElementText(index){
        console.log(this.specificChildElement(index).getText());
    }

}

module.exports = new Chapter2();