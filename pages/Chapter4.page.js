class Chapter4{
    get dropMenu () {return $('#dropdown')};
    get jsAlert () {return $("button[onclick='jsAlert()']")};
    get confirmJsalert () {return $("button[onclick='jsConfirm()']")};
    get jsAlertPrompt () {return $("button[onclick='jsPrompt()']")};
    get jsResult () {return $("#result")};
    optionDrop(index) {return $(`select#dropdown option[value='${index}']`)};
    
    dropDown(index){
        this.dropMenu.waitForDisplayed(5000);
        this.dropMenu.click();
        this.optionDrop(index).click();
        expect(this.optionDrop(index).isSelected()).to.equal(true);
    }

    jsAlerts(){
        this.jsAlert.waitForDisplayed(1000);
        this.jsAlert.click();
        expect(browser.getAlertText()).to.equal("I am a JS Alert");
        //Need to insert this command to not affect the next test.
        browser.acceptAlert();
    }

    jsAlertConfirm(){
        this.confirmJsalert.waitForDisplayed(1000);
        this.confirmJsalert.click();
        browser.acceptAlert();
        expect(this.jsResult.getText()).to.equal("You clicked: Ok");
    }

    jsAlertDismiss(){
        this.confirmJsalert.waitForDisplayed(1000);
        this.confirmJsalert.click();
        browser.dismissAlert();
        expect(this.jsResult.getText()).to.equal("You clicked: Cancel");
    }

    jsAlertDialog(text){
        this.jsAlertPrompt.waitForDisplayed(1000);
        this.jsAlertPrompt.click();
        browser.sendAlertText(text);
        browser.acceptAlert();
        expect(this.jsResult.getText()).to.equal("You entered: " + text);
    }
}

module.exports = new Chapter4();